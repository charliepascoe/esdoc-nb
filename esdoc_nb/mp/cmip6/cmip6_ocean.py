__author__ = 'eguil'
version = '0.0.1'

#
# This is the candidate standard vocabulary for CMIP6 ocean.

# Note that the classes and enums need to be mixed to make it possible to easily develop these
# ab initio in python. If we develop a different toolchain, we can split them.

# Note that vocab_status attribute is used for two reasons: the primary reason is to
# help the test system known what needs to be done in vocabulary development. Classes
# in the CMIP6 package without the attribute, or at the wrong level, are deemed to be
# incomplete. It is also used in the notebook meta system to indicate that classes
# carrying this attribute will not be abstract, and will not carry properties.

#
# Domain definition
#

def _ocean_domain():
    """ Characteristics of the ocean component model"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.scientific_domain',
        'constraints':[
            ('realm','value', 'Ocean'),
            ('simulates', 'include', ['cmip6.ocean_key_properties',
                                      'cmip6.ocean_seawater_properties',
                                      'cmip6.ocean_advection',
                                      'cmip6.ocean_lateral_physics',
                                      'cmip6.ocean_vertical_physics',
                                      'cmip6.ocean_boundaries',
                                      'cmip6.ocean_forcing']),
        ]
    }

#
# Processes follow
#

def _ocean_key_properties():
    """ Ocean key properties """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints':[
            ('name', 'value', 'Ocean Key Properties'),
            ('detailed_properties','include', [
                    'cmip6.ocean_basic_approximations',
                    'cmip6.ocean_list_of_prognostic_variables',
                    'cmip6.ocean_bathymetry_type'
                    'cmip6.ocean_bathymetry_reference_date'
                    'cmip6.ocean_tracer_damping'
                    'cmip6.ocean_momentum_damping'
                    'cmip6.ocean_horizontal_discretization_scheme'
                    'cmip6.ocean_horizontal_discretization_pole_treatment'
                    'cmip6.ocean_time_stepping_tracers'
                    'cmip6.ocean_barotropic_solver'
                    'cmip6.ocean_baroclinic_momentum'
                    'cmip6.ocean_time_step'
                    ]),
    ]
    }

def _ocean_seawater_properties():
    """ Ocean sea water properties """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints':[
            ('name', 'value', 'Ocean Sea Water Properties'),
            ('detailed_properties','include', [
                    'cmip6.ocean_equation_of_state',
                    'cmip6.ocean_specfic_heat',
                    'cmip6.ocean_freezing_point']),
    ]
    }

def _ocean_advection():
    """ Characteristics of ocean advection processes"""
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Ocean Advection'),
            ('detailed_properties','include',[
                    'cmip6.ocean_advection_momentum_scheme_name',
                    'cmip6.ocean_advection_momentum_scheme_type',
                    'cmip6.ocean_advection_tracer_lateral_scheme_type',
                    'cmip6.ocean_advection_tracer_lateral_monotonic_flux_limiter',
                    'cmip6.ocean_advection_tracer_vertical_scheme_type'
                    'cmip6.ocean_advection_tracer_vertical_scheme_monotonic_flux_limiter'
                    ]),
        ]
     }

def _ocean_lateral_physics():
    """ Characteristics of ocean lateral physics"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Ocean Lateral Physics'),
            ('detailed_properties','include',[
                    'cmip6.ocean_lateral_physics_momentum_direction',
                    'cmip6.ocean_lateral_physics_momentum_order',
                    'cmip6.ocean_lateral_physics_momentum_discretization',
                    'cmip6.ocean_lateral_physics_momentum_eddy_viscosity_coefficient_type',
                    'cmip6.ocean_lateral_physics_momentum_eddy_viscosity_coefficient_minimum',
                    'cmip6.ocean_lateral_physics_tracers_mesoscale_closure',
                    'cmip6.ocean_lateral_physics_tracers_direction',
                    'cmip6.ocean_lateral_physics_tracers_order',
                    'cmip6.ocean_lateral_physics_tracers_discretization',
                    'cmip6.ocean_lateral_physics_tracers_eddy_viscosity_coefficient_type',
                    'cmip6.ocean_lateral_physics_tracers_eddy_viscosity_coefficient_minimum',
                    'cmip6.ocean_lateral_physics_tracers_eddy_induced_velocity_scheme_type',
                    'cmip6.ocean_lateral_physics_tracers_eddy_induced_velocity_flux_type',
                    'cmip6.ocean_lateral_physics_tracers_eddy_induced_velocity_added_diffusivity',
                    ]),
        ]
    }

def _ocean_vertical_physics():
    """ Characteristics of ocean vertical physics"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Ocean Vertical Physics'),
            ('detailed_properties','include',[
                    'cmip6.ocean_vertical_physics_mixed_layer_tracers_scheme',
                    'cmip6.ocean_vertical_physics_mixed_layer_tracers_background_value',
                    'cmip6.ocean_vertical_physics_mixed_layer_momentum_scheme',
                    'cmip6.ocean_vertical_physics_mixed_layer_momentum_background_value',
                    'cmip6.ocean_vertical_physics_convection',
                    'cmip6.ocean_vertical_physics_tide_induced_mixing']),
                    'cmip6.ocean_vertical_physics_interior_tracers_scheme',
                    'cmip6.ocean_vertical_physics_interior_tracers_background_type',
                    'cmip6.ocean_vertical_physics_interior_momentum_scheme',
                    'cmip6.ocean_vertical_physics_interior_momentum_background_type',
        ]
    }

def _ocean_boundaries():
    """ Characteristics of ocean upper and lower boundaries"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Ocean Upper and Lower Boundaries'),
            ('detailed_properties','include',[
                    'cmip6.ocean_bottom_boundary_layer',
                    'cmip6.ocean_sill_overflow',
                    'cmip6.ocean_free_surface']),
        ]
    }

def _ocean_boundary_forcing():
    """ Characteristics of ocean boundary forcing"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name','value','Ocean Boundary Forcing'),
            ('detailed_properties','include',[
                    'cmip6.ocean_boundary_forcing_momemtum_bottom_friction',
                    'cmip6.ocean_boundary_forcing_momemtum_lateral_friction',
                    'cmip6.ocean_boundary_forcing_momemtum_surface_flux_correction',
                    'cmip6.ocean_boundary_forcing_tracers_geothermal_heating'
                    'cmip6.ocean_boundary_forcing_tracers_surface_flux_correction'
                    'cmip6.ocean_boundary_forcing_tracers_sunlight_penetration_type'
                    'cmip6.ocean_boundary_forcing_tracers_sunlight_ocean_color_dependent'
                    'cmip6.ocean_boundary_forcing_tracers_surface_salinity'
                    ]),
        ]
    }


#
# Sub process collection detail follows.
#
# We would not expect the comparator to go below comparisons at the sub-process
# level.
#
# Enums follow processes which use them. Not all process use them.
#
def _ocean_basic_approximations():
    """ """
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.process_detail',
        'properties':[
            ('selection','cmip6.ocean_possible_basic_approximation','1.N','Possible ocean basic approximations'),
        ],
        'constraints':[
            ('heading', 'value', 'Ocean basic approximation'),
            ('vocabulary', 'value', 'cmip6.ocean_possible_basic_approximation.%s' % version),
            ('selection','from','cmip6.ocean_possible_basic_approximation','1.N'),
        ]
    }

def _ocean_possible_basic_approximations():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members':[
            ('primitive equations', None),
            ('non-hydrostatic', None),
            ('Boussinesq', None),
            ('other', None),
        ]
    }

def _ocean_list_of_prognostic_variables():
    """ """
    return {
        'type': 'class',
        'vocab_status':'initial',
        'base': 'science.process_detail',
        'properties':[
            ('selection','cmip6.ocean_possible_list_of_prognostic_variables','1.N','Possible ocean prognostics variables'),
        ],
        'constraints':[
            ('heading', 'value', 'Ocean list of prognostic variables'),
            ('vocabulary', 'value', 'cmip6.ocean_possible_list_of_prognostic_variables.%s' % version),
            ('selection','from','cmip6.ocean_possible_list_of_prognostic_variables','1.N'),
        ]
    }

def _ocean_possible_list_of_prognostic_variables():
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members':[
            ('potential temperature', None),
            ('convervative temperature', None),
            ('salinity', None),
            ('U-velocity', None),
            ('V-velocity', None),
            ('W-velocity', None),
            ('SSH', None),
            ('other', None),
        ]
    }

#FIXME: --- to be continued --- 10/11/15 EG


def _sea_ice_ocean_to_ice_basal_heat_flux():
    """ """
    return {
        'type': 'class',
        'base': 'science.algorithm',
        'vocab_status': 'initial',
        'constraints': [
            ('heading', 'value',
                'Implementation detail for the ocean to ice basal heat flux.'),
            ('implementation_overview','value','(Replace: whether prescribed or calculated, and how)'),
            ('prognostic_variables', 'hidden'),
            ('diagnostic_variables', 'hidden'),
        ]
    }


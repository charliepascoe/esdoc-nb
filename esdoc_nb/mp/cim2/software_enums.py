
def programming_language():
    """ The set of terms which define programming languages used for earth
    system simulation.
    """
    return {
        'type': 'enum',
        'base': None,
        'doc': 'Computing language',
        'is_open': False,
        'members': [
            ('Fortran', None),
            ('C', None),
            ('C++', None),
            ('Python', None),
            ]
    }

def coupling_framework():
    """ The set of terms which define known coupling frameworks"""
    return {
        'type': 'enum',
        'base': None,
        'is_open': False,
        'members': [
            ('OASIS', None),
            ('OASIS3-MCT', None),
            ('ESMF', None),
            ('NUOPC', None),
            ('Bespoke', 'Customised coupler developed for this model'),
            ('Unknown', 'It is not known what/if-a coupler is used'),
            ('None','No coupler is used')
            ]
    }
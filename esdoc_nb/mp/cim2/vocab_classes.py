# An attempt to think about vocabs in a new way.
# file is NOT_CURRENTLY_USED (24/11/2014)
#
# In the original mind maps (from Moine et al 2013)
#       Black bold font denotes model components; 
#       Purple is for parameter groups; 
#       Blue is for conditional parameter groups; 
#       Brown is for leaf parameters expecting values; 
#           Black is for possible values for the leaf parameters; 
#           Red cross icons mark single choice (XOR), 
#           Green tick mark icons symbolize multiple choice (OR); 
#           Pencil icons are for free text entry (numeric entries are also possible)
#           Notebook icons ahead of a leaf parameter indicate that a definition 
#           is attached as a footnote.
#
#  

# A rudimentary constraint language
# Types of constraints     
#     '==' or '!=',a value, requires all of [(property, multiplicity_value),...]
#     'in', a (set of possible values), requires all of  [(property, multiplicity_value),...]

def constraint_types():
    ''' The set of constraints supported by the CIM '''
    return {
        'cimType':'constraintTypes',
        'type':'enum',
        'doc':'Constraints understood by the CIM',
        'properties': [
            ('equals','Property value must equal a particular choice'),
            ('in','Property value must lie in a particular set of values'),
            ('notEqual','Property value must not equal a particular choice'),
            ]
        }


def parameter_group():
    ''' Provides a grouping of properties with semantic meaning '''
    return {
        'cimType':'paramterGroup',
        'type':'class',
        'doc':'A set of parameters with something scientific in commmon',
        'properties': [
            ('name','str','1','Name of parameter group'),
            ('doc','str','0.1',
                'Documentation or definition of the scientific context for the group'),
            ('parameters','cim::property','1.N','Properties in the common grouping'),
            ('vocab_constraints','cim::constraints','0.N','Constraint Groups which could appear')
            
            ]
    }
    
def constraint():
    ''' A constraint and consequential extra information required '''
    return {
        'cimType':'constraint',
        'type':'class',
        'doc':'Extra set of parameters required given a property value constraint',
        'properties': [
            ('property_constraint','str','1','Name of property which is constrained'),
            ('constraintType','cim::constraintTypes','1','Type of constraint'),
            ('value_constraint','str','0.1','Value which satisfied constraint'),
            ('set_constraint','list','0.1','List of values for constraint satisfaction'),
            ('requires','cim::parameterGroup','1',
                'Parameter group required to be present if constraint is satisfied'),
            ]
        'constraints': [
            ('constraintType','==','equals',[('value_constraint','1'),]),
            ('constraintType','==','in',[('set_constraint','1'),]),
            ]
                
def property():
    ''' Used to define a property '''
    # Deliberately repeat the origintype in both property and value, allowing
    # a property to take values from different underlying (but compatible) 
    # choice set than the property choiceset ... typically over time.
    return {
        'cimType','property',
        'doc','A particular property of a component',
        'type','class',
        'properties':[
            ('name','str','1','The name of the property'),
            ('definition','str','0.1','A definition for the property'),
            ('originType','cim::propertyOriginType','1','The type of property expected'),
            ('origin','cim::choiceSet','0.1','If appropriate, the set of possible values'),
            ]
        'constraints': [
            ('originType','in',('controlled_choice','controlled'),[('origin','1').]),
            ]
    }


def property_value():
    ''' Used to hold property values '''
    # Deliberately repeat the origintype in both property and value, allowing
    # a property to take values from different underlying (but compatible) 
    # choice set than the property choiceset ... typically over time.
    return {
        'cimType':'propertyValue',
        'type':'class',
        'doc':'The value held for a specific property',
        'properties':[
            ('value','str','0.1','Actual value, if present'),
            ('originType','cim::propertyOrignType','0.1','Origin of value, if present'),
            ('units','str','0.1','Units, if present, and if appropriate'),
            ('origin','cim::choiceSet','0.1','If present, and if chosen from a set, the original set'),
            ]
        'constraints':[
            ('originType','==','user_numeric',[('units','1'),]),
            ('originType','in',('controlled_choice','controlled'),[('choiceSet','1'),])
        } 
        
def origin_type():
    ''' Used to identify the various sorts of allowable property value types'''
    return {
        'cimType':'propertyOriginType',
        'type':'enum',
        'doc':'The set of possible types of property values',
        'members':[
            ('controlled_choice','An exclusive choice from a set of options'),
            ('controlled','An commma separated set of choices from a set of options'),
            ('user_free_text','A free text value entered by a user'),
            ('user_numeric','A numeric value entered by a user'),
            ]
    }
    
def choice_set():
    ''' A set of vocabulary choices, along with documentation for
    the choices set'''
    return {
        'cimType':'choiceSet',
        'type':'class',
        'doc':'Set of choice constraints for a specfic property',
        'properties':[
            ('docstr','str','1','Documentation for the choice set'),
            ('options','cim::vocabOption','0.N','Options for the choice set'),
            ('optionType','cim::optypes','1','Type of choice'),
        ]
    }
    
def option_type():
    ''' Vocab defining types choice sets available '''
    return {
        'cimType':'optionType',
        'type':'enum',
        'doc':' Provides the set of possible selection modes, OR,XOR etc'
        'members': [
            ('XOR','Only one option can be chosen')
            ('AND','One or more options required')
            ]
            }
        
def vocab_option():
    ''' Provides a particular option choice with documentation.
    Semantically the same as an enum member.'''
    return {
        'cimtype':'vocabOption',
        'type':'class',
        'doc':'A particular vocabulary option',
        'properties':[
            ('value','str','1','Possible answer to a choice'),
            ('doc','str','0.1','Documentation for particular choice'),
            }
                

import os
import esdoc_nb.mp.cmip6.cmip6_seaice as seaice
import esdoc_nb.mp.cmip6.cmip6_atmosphere as atmosphere
import esdoc_nb.mp.cmip6.cmip6_ocean as ocean

from esdoc_nb.umlview import umlDiagram

__author__ = 'BNL28'


def get_cmip6_diagram():
    """ Get a umlDiagram set up for sea ice """
    d = umlDiagram()
    cmip6_pkg = {'seaice':seaice, 'ocean': ocean, 'atmosphere': atmosphere}
    d.factory.extend(cmip6_pkg)
    return d


def document_realm(package='seaice'):
    """ Generate documentation for a specfic package.
    We have three plots since the package has so many things to discuss
    """

    d = get_cmip6_diagram()
    d.setPackage(package, externals=True, no_enums=True)
    # remove all the classes which have ProcessDetail as their base
    # since there are so many of them, we'll do them in a subsequent diagram
    removals = []
    for c in d.classes2view:
        if 'ProcessDetail' in d.factory.base(c):
            removals.append(c)
    # one can't remove them as we go because the looping goes haywire, so we
    # identify them first, then remove them, and anyway we use them later.
    for c in removals:
        d.classes2view.remove(c)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_core' % package, nwidth=3, fmt='pdf')

    # now do the classes we couldn't fit on the previous page (all the
    # specialisations of 'ProcessDetail'
    d = get_cmip6_diagram()
    d.setClasses(removals)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_detail' % package, nwidth=3, fmt='pdf')

    # and finally do a plot of the vocabularies
    d = get_cmip6_diagram()
    d.setPackage(package, externals=False, no_classes=True)
    d.setAssociationEdges()
    d.plot(filebase='docs_cmip6/docs_%s_vocabs' % package, nwidth=3, fmt='pdf')

if __name__ == "__main__":

    # I have no idea why the current working directory is wrong
    print os.getcwd()
    os.chdir('../')

    document_realm('seaice')




